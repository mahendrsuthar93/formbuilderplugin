Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default=function(ctx) {
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }

 if (ctx.component.prefix || ctx.component.suffix) { ;
__p += '\n<div class="input-group">\n';
 } ;
__p += '\n';
 if (ctx.component.prefix) { ;
__p += '\n<div class="input-group-prepend" ref="prefix">\n  <span class="input-group-text">\n    ' +
((__t = (ctx.component.prefix)) == null ? '' : __t) +
'\n  </span>\n</div>\n';
 } ;
__p += '\n<' +
((__t = (ctx.input.type)) == null ? '' : __t) +
'\n  ref="' +
((__t = (ctx.input.ref ? ctx.input.ref : 'input')) == null ? '' : __t) +
'"\n  ';
 for (var attr in ctx.input.attr) { ;

  if(attr === "value"){
    
    switch(ctx.input.attr[attr]) {
      case "clientName":
      __p += 'placeholder="Client Name will come here" readonly data-name="clientName" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "assessorInitial":
      __p += 'placeholder="Assessor Initial will come here" readonly data-name="assessorInitial" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "assessorInitialList":
      __p += 'placeholder="Assessor Initial List will come here" readonly data-name="assessorInitialList" data-type="customvalues" data-sign="yes"';
      ctx.input.attr[attr] = '';
      break;
      case "assessorName":
      __p += 'placeholder="Assessor Name will come here" readonly data-name="assessorName" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "assessorSignature":
      __p += 'placeholder="Assessor Signature will come here" readonly data-name="assessorSignature" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "assessorSignatureList":
      __p += 'placeholder="Assessor Signature List will come here" readonly data-name="assessorSignatureList" data-type="customvalues" data-sign="yes"';
      ctx.input.attr[attr] = '';
      break;
      case "dateOfBirth":
      __p += 'placeholder="Client\'s Date Of Birth will come here" readonly data-name="dateOfBirth" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "todayDate":
      __p += 'placeholder="Today\'s date will come here" readonly data-name="todayDate" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "clientPharmacy":
      __p += 'placeholder="Client Pharmacy will come here" readonly data-name="clientPharmacy" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "clientAllergies":
      __p += 'placeholder="Client Allergies will come here" readonly data-name="clientAllergies" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "clientPhone":
      __p += 'placeholder="Client Phone will come here" readonly data-name="clientPhone" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "assessorSignature":
      __p += 'placeholder="Assessor Signature will come here" readonly data-name="assessorSignature" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "medicationDate":
      __p += 'placeholder="Medication Date will come here" readonly data-name="medicationDate" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "medicationDosage":
      __p += 'placeholder="Medication Dosage will come here" readonly data-name="medicationDosage" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "medicationFrequency":
      __p += 'placeholder="Medication Frequency will come here" readonly data-name="medicationFrequency" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "medicationGivenFor":
      __p += 'placeholder="Medication Given For will come here" readonly data-name="medicationGivenFor" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "medicationName":
      __p += 'placeholder="Medication Name will come here" readonly data-name="medicationName" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "medicationPrecautions":
      __p += 'placeholder="Medication Precautions will come here" readonly data-name="medicationPrecautions" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "medicactionRoute":
      __p += 'placeholder="Medicaction Route will come here" readonly data-name="medicactionRoute" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "medicationTime":
      __p += 'placeholder="Medication Time will come here" readonly data-name="medicationTime" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "medicationUsage":
      __p += 'placeholder="Medication Usage will come here" readonly data-name="medicationUsage" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "inappropriateDrugReactions":
      __p += 'placeholder="Medication inappropriate Drug Reactions will come here" readonly data-name="inappropriateDrugReactions" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "ineffectiveDrugTherapy":
      __p += 'placeholder="Medication ineffective Drug Therapy will come here" readonly data-name="ineffectiveDrugTherapy" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "foodDrugInteractions":
      __p += 'placeholder="Medication food Drug Interactions will come here" readonly data-name="foodDrugInteractions" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "medicationChangeDate":
      __p += 'placeholder="Medication Change Date will come here" readonly data-name="medicationChangeDate" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;

      //supervisory visit
      case "purposeOfVisit":
      __p += 'placeholder="Purpose of Visit will come here" readonly data-name="purposeOfVisit" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "visitDate":
      __p += 'placeholder="Date of Visit will come here" readonly data-name="visitDate" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "teachingAndTrainingProvided":
      __p += 'placeholder="List specifics will come here" readonly data-name="teachingAndTrainingProvided" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "skilledNursingCareClientSignature":
      __p += 'placeholder="Client Signature will come here" readonly data-name="skilledNursingCareClientSignature" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "skilledNursingNurseSignature":
      __p += 'placeholder="Nurse Signature will come here" readonly data-name="skilledNursingNurseSignature" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "skilledNursingCareClientSignatureDate":
      __p += 'placeholder="Client Signature Date will come here" readonly data-name="skilledNursingCareClientSignatureDate" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "skilledNursingNurseSignatureDate":
      __p += 'placeholder="Nurse Signature Date will come here" readonly data-name="skilledNursingNurseSignatureDate" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "generalNotes":
      __p += 'placeholder="Notes on Visit Outcome will come here" readonly data-name="generalNotes" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "timeIn":
      __p += 'placeholder="In Time will come here" readonly data-name="timeIn" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
       case "timeOut":
      __p += 'placeholder="Out Time will come here" readonly data-name="timeOut" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "nurseCompletingVisit":
      __p += 'placeholder="Nurse name will come here" readonly data-name="nurseCompletingVisit" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "bloodPressureInformation":
      __p += 'placeholder="Blood pressure reading will come here" readonly data-name="bloodPressureInformation" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "glucose":
      __p += 'placeholder="Glucose reading will come here" readonly data-name="glucose" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "currentPulseReading":
      __p += 'placeholder="Pulse reading value will come here" readonly data-name="currentPulseReading" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "pulseOxygenReading":
      __p += 'placeholder="PulseOx reading will come here" readonly data-name="pulseOxygenReading" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "currentRespirationsReading":
      __p += 'placeholder="Respiration reading will come here" readonly data-name="currentRespirationsReading" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "currentTemperatureReading":
      __p += 'placeholder="Temperature reading will come here" readonly data-name="currentTemperatureReading" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "bPNotes":
      __p += 'placeholder="Blood pressure Notes reading will come here" readonly data-name="bPNotes" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "glucoseNotes":
      __p += 'placeholder="Glucose Notes reading will come here" readonly data-name="glucoseNotes" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "pulseNotes":
      __p += 'placeholder="Pulse Notes reading value will come here" readonly data-name="pulseNotes" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "pulseOxygenNotes":
      __p += 'placeholder="PulseOx Notes reading will come here" readonly data-name="pulseOxygenNotes" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "respirationNotes":
      __p += 'placeholder="Respiration Notes reading will come here" readonly data-name="respirationNotes" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "temperatureNotes":
      __p += 'placeholder="Temperature Notes reading will come here" readonly data-name="temperatureNotes" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "skilledNursingMedication":
      __p += 'placeholder="Medication Management data will come here" readonly data-name="skilledNursingMedication" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "agencyLogoImage":
      __p += 'placeholder="Agency logo will come here" readonly data-name="agencyLogoImage" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;

      //caregiver profile
      case "cgfirstName":
      __p += 'placeholder="Caregiver first name will come here" readonly data-name="cgfirstName" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "cglastName":
      __p += 'placeholder="Caregiver last name will come here" readonly data-name="cglastName" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "cgoffice":
      __p += 'placeholder="Caregiver office notes will come here" readonly data-name="cgoffice" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "cggender":
      __p += 'placeholder="Caregiver gender will come here" readonly data-name="cggender" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "cgdob":
      __p += 'placeholder="Caregiver date of birth will come here" readonly data-name="cgdob" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "cgagencyLocation":
      __p += 'placeholder="Caregiver location will come here" readonly data-name="cgagencyLocation" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "maritalStatus":
      __p += 'placeholder="Caregiver marital status will come here" readonly data-name="maritalStatus" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "caregiverStatus":
      __p += 'placeholder="Caregiver enrolment status will come here" readonly data-name="caregiverStatus" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "cgspouseName":
      __p += 'placeholder="Caregiver spouse name will come here" readonly data-name="cgspouseName" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "cgisVeteran":
      __p += 'placeholder="Caregiver is veteran will come here" readonly data-name="cgisVeteran" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "cgstartOfCase":
      __p += 'placeholder="Caregiver hire date will come here" readonly data-name="cgstartOfCase" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "cgaddress":
      __p += 'placeholder="Caregiver address 1 will come here" readonly data-name="cgaddress" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "cgaddressLine1":
      __p += 'placeholder="Caregiver address 2 will come here" readonly data-name="cgaddressLine1" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "cgcity":
      __p += 'placeholder="Caregiver city will come here" readonly data-name="cgcity" data-type="customvalues"';
      ctx.input.attr[attr] = '';
      break;
      case "cgstate":
        __p += 'placeholder="Caregiver state will come here" readonly data-name="cgstate" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "cgcountry":
        __p += 'placeholder="Caregiver country will come here" readonly data-name="cgcountry" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "cgpostalCode":
        __p += 'placeholder="Caregiver zip code will come here" readonly data-name="cgpostalCode" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "cghomePhone":
        __p += 'placeholder="Caregiver home phone will come here" readonly data-name="cghomePhone" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "cgofficePhone":
        __p += 'placeholder="Caregiver office phone will come here" readonly data-name="cgofficePhone" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "cgmobilePhone":
        __p += 'placeholder="Caregiver mobile phone will come here" readonly data-name="cgmobilePhone" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "cgfax":
        __p += 'placeholder="Caregiver fax will come here" readonly data-name="cgfax" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "cgemail":
        __p += 'placeholder="Caregiver email address will come here" readonly data-name="cgemail" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "cgreferralType":
        __p += 'placeholder="Caregiver referral type will come here" readonly data-name="cgreferralType" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "cgreferralBy":
        __p += 'placeholder="Caregiver referred by will come here" readonly data-name="cgreferralBy" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "cgadpid":
        __p += 'placeholder="Caregiver adp id will come here" readonly data-name="cgadpid" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "cgFullName":
        __p += 'placeholder="Caregiver full name will come here" readonly data-name="cgFullName" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "cgFullAddress":
        __p += 'placeholder="Caregiver full address will come here" readonly data-name="cgFullAddress" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      //employee profile
      case "empFirstName":
        __p += 'placeholder="Employee first name will come here" readonly data-name="empFirstName" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "empLastName":
        __p += 'placeholder="Employee last name will come here" readonly data-name="empLastName" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "empGender":
        __p += 'placeholder="Employee gender will come here" readonly data-name="empGender" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "empDateofJoining":
        __p += 'placeholder="Employee date of joining will come here" readonly data-name="empDateofJoining" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "empAddress1":
        __p += 'placeholder="Employee address line 1 will come here" readonly data-name="empAddress1" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "empAddress2":
        __p += 'placeholder="Employee address line 2 will come here" readonly data-name="empAddress2" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "empCity":
        __p += 'placeholder="Employee city will come here" readonly data-name="empCity" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "empState":
        __p += 'placeholder="Employee state will come here" readonly data-name="empState" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "empCountry":
        __p += 'placeholder="Employee country will come here" readonly data-name="empCountry" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "empZipCode":
        __p += 'placeholder="Employee zip code will come here" readonly data-name="empZipCode" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "empMobilePhone":
        __p += 'placeholder="Employee mobile phone will come here" readonly data-name="empMobilePhone" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "empEmail":
        __p += 'placeholder="Employee email will come here" readonly data-name="empEmail" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "empFullName":
        __p += 'placeholder="Employee full name will come here" readonly data-name="empFullName" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
      case "empFullAddress":
        __p += 'placeholder="Employee full name will come here" readonly data-name="empFullAddress" data-type="customvalues"';
        ctx.input.attr[attr] = '';
        break;
    }
    
  }
  
__p += '\n  ' +
((__t = (attr)) == null ? '' : __t) +
'="' +
((__t = (ctx.input.attr[attr])) == null ? '' : __t) +
'"\n  ';
 } ;
__p += '\n>' +
((__t = (ctx.input.content)) == null ? '' : __t) +
'</' +
((__t = (ctx.input.type)) == null ? '' : __t) +
'>\n';
 if (ctx.component.showCharCount) { ;
__p += '\n<span class="text-muted pull-right" ref="charcount"></span>\n';
 } ;
__p += '\n';
 if (ctx.component.showWordCount) { ;
__p += '\n<span class="text-muted pull-right" ref="wordcount"></span>\n';
 } ;
__p += '\n';
 if (ctx.component.suffix) { ;
__p += '\n<div class="input-group-append" ref="suffix">\n  <span class="input-group-text">\n    ' +
((__t = (ctx.component.suffix)) == null ? '' : __t) +
'\n  </span>\n</div>\n';
 } ;
__p += '\n';
 if (ctx.component.prefix || ctx.component.suffix) { ;
__p += '\n</div>\n';
 } ;
__p += '\n';
return __p
}